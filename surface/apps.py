import sys
from os import environ

from django.apps import AppConfig
from django.core.management import call_command


class AttackSurfaceConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "surface"

    def ready(self) -> None:
        super().ready()

        if "runserver" in sys.argv and (data_file := environ["DATA_FILE"]):
            call_command("load_data", clean=True, data_file=data_file)
