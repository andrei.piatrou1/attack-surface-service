from django.http import HttpResponseBadRequest

from rest_framework import generics
from rest_framework.response import Response

from .serializers import AttackSurfaceParamsSerializer
from .stats import get_stats
from .storages import storage


class AttackSurfaceView(generics.GenericAPIView):
    serializer_class = AttackSurfaceParamsSerializer

    def get(self, request):
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        dest_vm_id = serializer.validated_data["vm_id"]
        if not storage.vm_exists(vm_id=dest_vm_id):
            return HttpResponseBadRequest(f"Unknown VM: {dest_vm_id}")

        source_vms = storage.vm_attack_surface(dest_vm_id)

        return Response(source_vms)


class StatsView(generics.GenericAPIView):
    def get(self, request):
        stats = get_stats()
        stats["vm_count"] = storage.vm_count()

        return Response(stats)
