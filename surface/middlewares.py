import time

from .stats import update_stats


class TimingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        start = time.time()

        response = self.get_response(request)

        elapsed = time.time() - start

        update_stats(elapsed)

        return response
