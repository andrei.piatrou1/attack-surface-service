from unittest.mock import ANY

from django.test import TestCase
from django.urls import reverse

from rest_framework import status

from surface import stats
from surface.storages import storage


class AttackSurfaceViewTest(TestCase):
    def setUp(self):
        storage.clear()

    def test_attack_of_single_vm_returns_empty_list(self):
        storage.load_vms([{"vm_id": "vm-1", "name": "jira", "tags": ["ci"]}])

        response = self.client.get(reverse("attack_surface"), {"vm_id": "vm-1"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_attack_of_unknown_vm_returns_400(self):
        response = self.client.get(reverse("attack_surface"), {"vm_id": "123"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_response_without_query_returns_400(self):
        response = self.client.get(reverse("attack_surface"))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_attack_of_linked_vm_returns_source_vms(self):
        vms = [
            {"vm_id": "vm-1", "name": "jira_server", "tags": ["ci", "dev"]},
            {"vm_id": "vm-2", "name": "bastion", "tags": ["ssh"]},
        ]
        fw_rules = [{"fw_id": "fw-82af742", "source_tag": "ssh", "dest_tag": "dev"}]
        storage.load_vms(vms)
        storage.load_fw_rules(fw_rules)

        response = self.client.get(reverse("attack_surface"), {"vm_id": "vm-1"})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0], "vm-2")


class StatsTest(TestCase):
    def setUp(self):
        stats.clear_stats()

    def test_get_stats_returns_empty_dict_when_stats_is_not_initialized(self):
        self.assertEqual(stats.get_stats(), {})

    def test_get_stats_returns_stats_of_one_request(self):
        stats.update_stats(0.2)
        self.assertEqual(
            stats.get_stats(), {"request_count": 1, "average_request_time": 0.2}
        )

    def test_get_stats_returns_correct_average(self):
        stats.update_stats(0.002)
        stats.update_stats(0.003)
        stats.update_stats(0.004)
        stats.update_stats(0.005)
        stats.update_stats(0.006)

        self.assertEqual(
            stats.get_stats(), {"request_count": 5, "average_request_time": 0.004}
        )


class StatsViewTest(TestCase):
    def setUp(self):
        stats.clear_stats()
        storage.clear()

    def test_get_stats_returns_empty_dict_when_stats_is_not_initialized(self):
        storage.load_vms([{"vm_id": "vm-a211de", "name": "jira", "tags": ["ci"]}])

        # this is the first call, request count and avg response time unknown
        response = self.client.get(reverse("stats"))
        self.assertEqual(response.json(), {"vm_count": 1})

        # this is the second call, request count and avg response time should
        # be returned for requests prior to this one
        response = self.client.get(reverse("stats"))
        self.assertEqual(
            response.json(),
            {"vm_count": 1, "request_count": 1, "average_request_time": ANY},
        )
