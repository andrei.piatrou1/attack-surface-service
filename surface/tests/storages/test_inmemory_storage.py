from django.test import TestCase

from surface.storages import InMemoryRuleStorage


class InMemoryStorageTest(TestCase):
    def setUp(self):
        self.storage = InMemoryRuleStorage()

    def tearDown(self):
        self.storage.clear()

    def test_input_file_loads_ok(self):
        vms = [
            {"vm_id": "vm-1", "name": "jira_server", "tags": ["ci", "dev"]},
            {"vm_id": "vm-2", "name": "bastion", "tags": ["ssh"]},
        ]
        fw_rules = [{"fw_id": "fw-1", "source_tag": "ssh", "dest_tag": "dev"}]
        self.storage.load_vms(vms)
        self.storage.load_fw_rules(fw_rules)

        self.assertEqual(self.storage.vm_count(), 2)

    def test_attack_of_single_vm_returns_empty_list(self):
        self.storage.load_vms([{"vm_id": "vm-1", "name": "jira", "tags": ["ci"]}])
        self.assertEqual(len(self.storage.vm_attack_surface(vm_id="vm-1")), 0)

    def test_storage_knows_single_vm_without_tags(self):
        self.storage.load_vms([{"vm_id": "vm-1", "name": "jira", "tags": []}])
        self.assertTrue(self.storage.vm_exists(vm_id="vm-1"))

    def test_attack_of_unknown_vm_returns_400(self):
        self.assertEqual(len(self.storage.vm_attack_surface(vm_id="123")), 0)

    def test_attack_of_linked_vm_returns_source_vms(self):
        vms = [
            {"vm_id": "vm-1", "name": "jira_server", "tags": ["ci", "dev"]},
            {"vm_id": "vm-2", "name": "bastion", "tags": ["ssh"]},
        ]
        fw_rules = [{"fw_id": "fw-82af742", "source_tag": "ssh", "dest_tag": "dev"}]
        self.storage.load_vms(vms)
        self.storage.load_fw_rules(fw_rules)

        self.assertEqual(self.storage.vm_attack_surface(vm_id="vm-1"), ["vm-2"])

    def test_virtual_machines_with_the_same_tag_cant_access_each_other(self):
        vms = [
            {"vm_id": "vm-1", "name": "jira_server", "tags": ["ci", "dev"]},
            {"vm_id": "vm-2", "name": "bastion", "tags": ["ssh", "dev"]},
        ]
        fw_rules = [{"fw_id": "fw-82af742", "source_tag": "ssh", "dest_tag": "dev"}]

        self.storage.load_vms(vms)
        self.storage.load_fw_rules(fw_rules)

        # Virtual machines with the same tag can't access each other
        self.assertEqual(self.storage.vm_attack_surface(vm_id="vm-1"), ["vm-2"])

        # There's a fw rule that allows traffic ssh -> dev, but the vm
        # can't attack itself.
        self.assertEqual(self.storage.vm_attack_surface(vm_id="vm-2"), [])

    def test_firewall_rules_are_not_transitive(self):
        vms = [
            {"vm_id": "vm-a", "name": "apple", "tags": ["apple"]},
            {"vm_id": "vm-b", "name": "banana", "tags": ["banana"]},
            {"vm_id": "vm-c", "name": "cherry", "tags": ["cherry"]},
        ]
        fw_rules = [
            {"fw_id": "fw-1", "source_tag": "apple", "dest_tag": "banana"},
            {"fw_id": "fw-2", "source_tag": "banana", "dest_tag": "cherry"},
        ]
        self.storage.load_vms(vms)
        self.storage.load_fw_rules(fw_rules)

        # NB: no vm-a on the surface!
        self.assertEqual(self.storage.vm_attack_surface(vm_id="vm-c"), ["vm-b"])
