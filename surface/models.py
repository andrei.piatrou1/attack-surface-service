from django.db import models


class Tag(models.Model):
    """A tag that can be added to any virtual machine."""

    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return self.name


class VirtualMachine(models.Model):
    vm_id = models.CharField(max_length=32, unique=True)

    name = models.CharField(max_length=128)
    tags = models.ManyToManyField(Tag, related_name="virtual_machines")


class FirewallRule(models.Model):
    fw_id = models.CharField(max_length=32, unique=True)

    source_tag = models.CharField(max_length=32)
    dest_tag = models.CharField(max_length=32)
