from rest_framework import serializers


class AttackSurfaceParamsSerializer(serializers.Serializer):
    vm_id = serializers.CharField(required=True)

    class Meta:
        fields = ["vm_id"]
