from django.core.cache import cache

REQUEST_COUNT_KEY = "request_count"
AVERAGE_REQUEST_TIME_KEY = "average_request_time"


def update_stats(last_request_time: float) -> None:
    """Update statistics with a new value.
    1. Increments request count by one.
    2. Updates incremental average of request time.
        The formula is as follows:
        new_average = old_average + (new_value - old_average) / new_sample_count
    """
    if cache.get(REQUEST_COUNT_KEY) is None:
        # stats key is missing - looks like it's our first request
        # in this case just initialize our keys
        cache.set(REQUEST_COUNT_KEY, 1)
        cache.set(AVERAGE_REQUEST_TIME_KEY, last_request_time)
    else:
        new_request_count = cache.incr(REQUEST_COUNT_KEY)

        old_average_request_time = cache.get(AVERAGE_REQUEST_TIME_KEY)
        new_average_request_time = (
            old_average_request_time
            + (last_request_time - old_average_request_time) / new_request_count
        )
        cache.set(AVERAGE_REQUEST_TIME_KEY, new_average_request_time)


def get_stats() -> dict:
    """Get the statistics.

    If the stats is not available, returns empty dict."""
    return cache.get_many([REQUEST_COUNT_KEY, AVERAGE_REQUEST_TIME_KEY])


def clear_stats() -> None:
    cache.clear()
