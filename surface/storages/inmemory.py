from collections import defaultdict

from .base import AbstractRuleStorage


class InMemoryRuleStorage(AbstractRuleStorage):
    def __init__(self):
        self.vm_by_tag: dict[str, set[str]] = {}
        self.attack_surface: dict[str, set[str]] = {}
        self.known_vms = set()

    def vm_exists(self, vm_id: str) -> bool:
        return vm_id in self.known_vms

    def vm_attack_surface(self, vm_id: str) -> list[str]:
        return list(self.attack_surface.get(vm_id, []))

    def vm_count(self) -> int:
        return len(self.known_vms)

    def load_vms(self, vms: list[dict[str, str]]) -> None:
        for vm_data in vms:
            for tag_name in vm_data["tags"]:
                self.vm_by_tag.setdefault(tag_name, set()).add(vm_data["vm_id"])

            self.known_vms.add(vm_data["vm_id"])

    def load_fw_rules(self, fw_rules: list[dict[str, str]]) -> None:
        surface = defaultdict(set)

        for fw_rule in fw_rules:
            source_vms = self.vm_by_tag.get(fw_rule["source_tag"], set())
            dest_vms = self.vm_by_tag.get(fw_rule["dest_tag"], set())

            for dest_vm in dest_vms:
                surface[dest_vm] |= source_vms - {dest_vm}

        self.attack_surface = dict(surface)

    def clear(self) -> None:
        self.vm_by_tag = {}
        self.attack_surface = {}
        self.known_vms = set()
