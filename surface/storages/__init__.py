from django.conf import settings

from .database import DatabaseRuleStorage
from .inmemory import InMemoryRuleStorage


def get_storage():
    if settings.USE_DB_STORAGE:
        return DatabaseRuleStorage()

    return InMemoryRuleStorage()


storage = get_storage()
