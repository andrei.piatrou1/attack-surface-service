import abc


class AbstractRuleStorage(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def vm_exists(self, vm_id: str) -> bool:
        ...

    @abc.abstractmethod
    def vm_attack_surface(self, vm_id: str) -> list[str]:
        ...

    @abc.abstractmethod
    def vm_count(self) -> int:
        ...

    @abc.abstractmethod
    def load_vms(self, vms: list[dict[str, str]]) -> None:
        ...

    @abc.abstractmethod
    def load_fw_rules(self, fw_rules: list[dict[str, str]]) -> None:
        ...

    @abc.abstractmethod
    def clear(self) -> None:
        ...
