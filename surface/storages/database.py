from surface.models import FirewallRule, Tag, VirtualMachine

from .base import AbstractRuleStorage


def chunker(seq, size):
    for pos in range(0, len(seq), size):
        yield seq[pos : pos + size]


BATCH_SIZE = 500


class DatabaseRuleStorage(AbstractRuleStorage):
    def vm_attack_surface(self, vm_id: str) -> list[str]:
        # 1. Build a queryset for tags of this VM
        dest_vm_tags = (
            Tag.objects.filter(virtual_machines__vm_id=vm_id)
            .values_list("name", flat=True)
            .all()
        )

        # 2. Lookup tag names of source VM
        source_tags = FirewallRule.objects.filter(
            dest_tag__in=dest_vm_tags
        ).values_list("source_tag", flat=True)

        # 3. Find all VMs with source tags
        source_vms = VirtualMachine.objects.filter(
            tags__name__in=source_tags
        ).values_list("vm_id", flat=True)

        return list(set(source_vms) - {vm_id})

    def vm_exists(self, vm_id: str) -> bool:
        return VirtualMachine.objects.filter(vm_id=vm_id).exists()

    def vm_count(self) -> int:
        return VirtualMachine.objects.count()

    def load_vms(self, vms: list[dict[str, str]]) -> None:
        # Mapping to store DB ids of tag objects by their name.
        tag_ids = {}

        for batch in chunker(vms, BATCH_SIZE):
            vms = []
            tags = []

            for vm_data in batch:
                vms.append(
                    VirtualMachine(
                        vm_id=vm_data["vm_id"],
                        name=vm_data["name"],
                    )
                )

                tags.extend(
                    [
                        Tag(name=tag_name)
                        for tag_name in vm_data["tags"]
                        if tag_name not in tag_ids
                    ]
                )

            VirtualMachine.objects.bulk_create(vms, ignore_conflicts=True)
            # Mapping where we store DB ids of virtual machine objects
            # we just created.  We'll use them later to link tags in bulk.
            vm_ids = dict(
                VirtualMachine.objects.filter(
                    vm_id__in=[vm["vm_id"] for vm in batch]
                ).values_list("vm_id", "pk")
            )

            if tags:
                Tag.objects.bulk_create(tags, ignore_conflicts=True)

                tag_ids.update(
                    dict(
                        Tag.objects.filter(
                            name__in=[tag.name for tag in tags]
                        ).values_list("name", "pk")
                    )
                )

            # Create links between tag objects and virtual machines.
            tag_links = []
            for vm_data in batch:
                for tag_name in vm_data["tags"]:
                    tag_links.append(
                        VirtualMachine.tags.through(
                            tag_id=tag_ids[tag_name],
                            virtualmachine_id=vm_ids[vm_data["vm_id"]],
                        )
                    )
            VirtualMachine.tags.through.objects.bulk_create(
                tag_links, ignore_conflicts=True
            )

    def load_fw_rules(self, fw_rules: list[dict[str, str]]) -> None:
        for batch in chunker(fw_rules, BATCH_SIZE):
            for fw_rule in batch:
                FirewallRule.objects.bulk_create(
                    [
                        FirewallRule(
                            fw_id=fw_rule["fw_id"],
                            source_tag=fw_rule["source_tag"],
                            dest_tag=fw_rule["dest_tag"],
                        )
                    ],
                    ignore_conflicts=True,
                )

    def clear(self) -> None:
        for model in [VirtualMachine, FirewallRule, Tag]:
            model.objects.filter().delete()
