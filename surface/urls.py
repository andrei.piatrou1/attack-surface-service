"""Attack surface app URL Configuration"""
from django.urls import path

from . import views

urlpatterns = [
    path("attack", views.AttackSurfaceView.as_view(), name="attack_surface"),
    path("stats", views.StatsView.as_view(), name="stats"),
]
