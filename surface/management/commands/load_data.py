import json
import sys
import time

from django.core.management import BaseCommand, CommandError
from django.db import transaction

from surface.storages import storage


class Command(BaseCommand):
    help = "Loads data file"

    def add_arguments(self, parser):
        parser.add_argument(
            "--data-file",
            action="store",
            dest="data_file",
            help=(
                "JSON document describing the cloud environment of a customer. "
                "Use '-' character if you pass file contents via stdin."
            ),
        )
        parser.add_argument(
            "--clean",
            action="store_true",
            dest="clean",
            default=False,
            help="Delete all existing records before loading the data",
        )

    @transaction.atomic
    def handle(self, **options):
        if not options["data_file"]:
            raise CommandError("Please provide a path to JSON document to load")

        if (data_file := options["data_file"]) == "-":
            data = json.load(sys.stdin)
        else:
            with open(data_file) as fp:
                data = json.load(fp)

        if options["clean"]:
            self.stdout.write(
                "WARNING! You specified the --clean flag, "
                "so all current vms, fw_rules and tags will be destroyed!"
            )
            storage.clear()

        self.stdout.write("Loading vms... ", ending="")
        start = time.time()
        storage.load_vms(data["vms"])
        self.stdout.write(f"OK! (took {time.time() - start:.2f} seconds)")

        self.stdout.write("Loading fw rules... ", ending="")
        start = time.time()
        storage.load_fw_rules(data["fw_rules"])
        self.stdout.write(f"OK! (took {time.time() - start:.2f} seconds)")
