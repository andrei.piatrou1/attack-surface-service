# Attack Surface service

## Description

This is a test project made for applying for a job position at one specific company.

The project is a service that a customer can query and get the attack surface of a VM -
meaning which other virtual machines in the account can access and potentially attack it.


## How to run

Project is tested on Linux machine, specifically Ubuntu 20.04.

The project is using [Taskfile][1] as a lightweight Makefile replacement.

There are two ways to run the application: inside a docker container (preferred)
or install dependencies locally.


## Start using Docker

First, ensure you have docker installed:

```bash
$ docker --version
Docker version 19.03.3, build a872fc2
```

Build the docker image:

    $ ./Taskfile docker-build

Run the migrations:

    $ ./Taskfile docker-migrate

Start the server:

    $ ./Taskfile docker-service data/input-1.json

This will start the server listening on port 80 locally (please ensure that port
is not occupied before starting the server.)

The server will load the input file specified in the first argument on startup.


## Alternative: Start using virtualenv

Ensure you have Python 3.9 (the project is tested on this version, but others may also work.)

Create the virtual environment and activate it:

```
$ python3 -m venv venv
$ source ./venv/bin/activate
```
Or, you can use [virtualenvwrapper][2] to automate virtualenv creation.

Install project's dependencies:

    $ ./Taskfile install

Run the migrations:

    $ ./Taskfile migrate

Start the server:

    $ DATA_FILE=data/input-1.json ./Taskfile service

This will start the server listening on port 80 locally (please ensure that port
is not occupied before starting the server.)

The server will load the input file specified in `DATA_FILE` environment variable on startup.


## Contributing
Start the project using one of the ways above.

After you do some changes, run linters and tests:

Via Docker:

    $ ./Taskfile docker-lint
    $ ./Taskfile docker-test

Or via virtualenv:

    $ ./Taskfile lint
    $ ./Taskfile test

## License
MIT

# A few notes on implementation

1. This project supports two different storages of the data required to serve
   requests: in-memory (fast) and via database (allows persistence).  You can switch
   between them via `USE_DB_STORAGE` setting.  In-memory storage is the default.

   In-memory storage pre-computes the attack surface and serves requests very efficiently.
   DB storage is currently not optimized, it uses classic modelling of many-to-many relationships.

1. Pre-computation is performed on startup and takes some time. If it's an issue,
   I can imagine loading the data as is to serve requests as early as possible,
   and then fire a background thread that will pre-compute the surface of all vms.

1. For the sake of simplicity, we use SQLite as the database engine (no need to mess with
   another container for full-fledged DB like Postgres.)

1. Due to the bullet above, VM tags are stored as a classic Many-to-Many relation between
   VMs and Tags and denormalized tables.  This makes queries a bit more complicated and
   less performant, but can be understood and extended more easily by anyone who is
   familiar with Django.

   The `attack` endpoint invokes just one query, and it's like this:

    ```SQL
    SELECT "surface_virtualmachine"."vm_id"
    FROM "surface_virtualmachine"
    INNER JOIN "surface_virtualmachine_tags" ON ("surface_virtualmachine"."id" = "surface_virtualmachine_tags"."virtualmachine_id")
    INNER JOIN "surface_tag" ON ("surface_virtualmachine_tags"."tag_id" = "surface_tag"."id")
    WHERE "surface_tag"."name" IN (
        SELECT V0."source_tag"
        FROM "surface_firewallrule" V0
        WHERE V0."dest_tag" IN (
            SELECT U0."name"
            FROM "surface_tag" U0
            INNER JOIN "surface_virtualmachine_tags" U1 ON (U0."id" = U1."tag_id")
            INNER JOIN "surface_virtualmachine" U2 ON (U1."virtualmachine_id" = U2."id")
            WHERE U2."name" = 'vm-4ae2f2be7'
        )
    )
    ```

   If we really need to make the queries faster, we can use PostgreSQL database with its
   support for Array field and store tags as an array, and GIN index over that field.
   This will make the queries index-only.

1. The data files are loaded via special command [load_data.py][3]

   This command takes additional argument `--clean` which controls whether we need to
   delete all entries from DB before loading new data. This flag is turned on
   in Taskfile commands.

   To make DB storage fast for larger datasets, `bulk_create` method is used
   where possible. A synthetically generated test file with 100K VMs on my
   machine loads in about 20 seconds (~5K VMs/s). The in-memory storage loads
   this file in about 5 seconds.

   For a persistent kind of storage the data can be pre-loaded via this command, and
   not take time on startup.  For ephemeral storage (i.e. in-memory) the data
   must be loaded on server startup.

1. There was a requirement to store statistics (metrics) of endpoints performance
   and number of queries so far.  We store this information in standard Django in-memory
   cache backend. Statistics is collected via special [middleware][4].
   If we ever need to scale the app, we can just switch to other, server-based
   backend such as memcached or redis, and stats will be automatically collected
   across all our instances.

1. If we need to scale the app more seriously, we can:
      1. Switch to some full-fledged DB (e.g. Postgres) and optimize the data structure;
      1. Or: use non-relational DB like Redis;
      1. Use application server (e.g. uwsgi) for serving the app;
      1. Use some other specialized tool to collect metrics (e.g. prometheus + grafana);
      1. As a last resort, rewrite in other language (e.g. Go).



[1]: https://github.com/adriancooney/Taskfile
[2]: https://virtualenvwrapper.readthedocs.io
[3]: https://gitlab.com/andrei.piatrou1/attack-surface-service/-/blob/main/surface/management/commands/load_data.py
[4]: https://gitlab.com/andrei.piatrou1/attack-surface-service/-/blob/main/surface/middlewares.py
