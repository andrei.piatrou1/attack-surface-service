FROM python:3.9-slim
RUN apt-get update && apt-get install -y make git && rm -rf /var/lib/apt/lists/*
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code

COPY requirements/*.txt requirements/
COPY Taskfile .

RUN ./Taskfile install
