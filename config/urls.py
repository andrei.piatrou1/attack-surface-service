"""Attack surface project URL Configuration"""

from django.urls import include, path

urlpatterns = [
    path("api/v1/", include("surface.urls")),
]
